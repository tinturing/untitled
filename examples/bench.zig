/// Here we have an entirely meaningless benchmark.
/// Try it out like this:
///
///     $ zig build -Drelease-fast
///     $ time zig-out/bin/bench 1000000
///
const std = @import("std");

const untitled = @import("untitled");

const stdout = std.io.getStdOut();

pub fn main() anyerror!void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.detectLeaks();
    const allocator = gpa.allocator();

    const args = try std.process.argsAlloc(allocator);
    defer std.process.argsFree(allocator, args);

    if (args.len < 2) {
        return error.MissingArgument;
    }

    const count = try std.fmt.parseInt(usize, args[1], 10);

    var world = untitled.World(&.{i32}).init(allocator);
    defer world.deinit();

    // NOTE Unsurprisingly, preallocating the relevant `ArrayList`s is faster.
    // try world.storage[0].entities.dense.ensureTotalCapacity(count);
    // try world.storage[0].entities.sparse.ensureTotalCapacity(count);
    // try world.storage[0].values.ensureTotalCapacity(count);

    var i: i32 = 0;
    while (i < count) : (i += 1) {
        _ = try world.spawn(.{i});
    }

    var sum: i64 = 0;

    var query = world.queryPtr(&.{i32});

    while (query.next()) |current| {
        sum += current.components[0].*;
    }

    try stdout.writer().print("{d}\n", .{sum});
}
