/// Try this example.
///
///     $ zig build run
///
const std = @import("std");

const untitled = @import("untitled");

const Position = struct { x: u32, y: u32 };
const Health = i32;
const Visible = struct {};

const World = untitled.World(&.{ Position, Health, Visible });

pub fn main() anyerror!void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.detectLeaks();
    const allocator = gpa.allocator();

    var world = World.init(allocator);
    defer world.deinit();

    _ = try world.spawn(.{ @as(Health, 80), Position{ .x = 0, .y = 0 }, Visible{} });
    _ = try world.spawn(.{ Position{ .x = 1, .y = 1 }, Visible{} });
    _ = try world.spawn(.{Position{ .x = 2, .y = 1 }});
    _ = try world.spawn(.{ @as(Health, 100), Position{ .x = 1, .y = 4 }, Visible{} });

    var query = world.queryPtr(&.{ Position, Visible });

    while (query.next()) |result| {
        const pos = result.components[0].*;

        std.log.info("an entity [{d}] is visible at ({d}, {d})", .{ result.entity, pos.x, pos.y });
    }
}
