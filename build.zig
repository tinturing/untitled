const std = @import("std");

pub fn build(b: *std.build.Builder) void {
    const mode = b.standardReleaseOptions();
    const target = b.standardTargetOptions(.{});

    const lib = b.addStaticLibrary("untitled", "untitled.zig");
    lib.setBuildMode(mode);
    lib.install();

    const pkg = std.build.Pkg{
        .name = "untitled",
        .source = .{ .path = "untitled.zig" },
    };

    {
        const entity_tests = b.addTest("src/entity.zig");
        entity_tests.setBuildMode(mode);

        const component_tests = b.addTest("src/component.zig");
        component_tests.setBuildMode(mode);

        const world_tests = b.addTest("src/world.zig");
        world_tests.setBuildMode(mode);

        const sparse_tests = b.addTest("src/sparse.zig");
        sparse_tests.setBuildMode(mode);

        const step = b.step("test", "Run the test suite");
        step.dependOn(&entity_tests.step);
        step.dependOn(&component_tests.step);
        step.dependOn(&world_tests.step);
        step.dependOn(&sparse_tests.step);
    }

    {
        const exe = b.addExecutable("run", "examples/run.zig");
        exe.setTarget(target);
        exe.setBuildMode(mode);
        exe.install();
        exe.addPackage(pkg);

        const cmd = exe.run();
        cmd.step.dependOn(b.getInstallStep());
        if (b.args) |args| {
            cmd.addArgs(args);
        }

        const step = b.step("run", "Run the example");
        step.dependOn(&cmd.step);
    }

    {
        const exe = b.addExecutable("bench", "examples/bench.zig");
        exe.setTarget(target);
        exe.setBuildMode(mode);
        exe.install();
        exe.addPackage(pkg);

        const cmd = exe.run();
        cmd.step.dependOn(b.getInstallStep());
        if (b.args) |args| {
            cmd.addArgs(args);
        }

        const step = b.step("bench", "Run the benchmark");
        step.dependOn(&cmd.step);
    }
}
