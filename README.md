# Untitled ECS library

An Entity-Component-System library.
Written in [Zig](https://ziglang.org).

The library makes extensive use of Zig's `comptime` feature.

Currently this is more of a proof-of-concept.

## Example

This is a very basic example of how to use `untitled`.

Alternatively, you can find this example at `examples/run.zig`.

Run it with:

```sh
zig build run
```

---

Define some components and a `World` to store them in.

> Note: Components are just plain old Zig types.

```zig
const Position = struct { x: u32, y: u32 };
const Health = i32;
const Visible = struct {};

const World = untitled.World(&.{ Position, Health, Visible });

var world = World.init(allocator);
defer world.deinit();
```

Create entities with initial components.

```zig
const player = try world.spawn(.{ @as(Health, 80), Position{ .x = 0, .y = 0 }, Visible{} });
const obstacle = try world.spawn(.{ Position{ .x = 1, .y = 1 }, Visible{} });
const trap = try world.spawn(.{Position{ .x = 2, .y = 1 }});
const enemy = try world.spawn(.{ @as(Health, 100), Position{ .x = 1, .y = 4 }, Visible{} });
```

> Note: If you copy paste this example into a file, Zig will probably complain about unused variables.
> You'll have to change the assignments to use `discards` instead.
>
> ```zig
> _ = try world.spawn(.{Position{ .x = 1, .y = 1 }});
> ```

Find the entities that have some specified components.

```zig
var query = world.query(&.{ Position, Visible });

while (query.next()) |result| {
    const pos = result.components[0].*;

    std.log.info("an entity [{d}] is visible at ({d}, {d})", .{ result.entity, pos.x, pos.y });
}

// info: an entity [0] is visible at (0, 0)
// info: an entity [1] is visible at (1, 1)
// info: an entity [3] is visible at (1, 4)
```
