const std = @import("std");

const Entity = @import("entity.zig").Entity;
const EntitySet = @import("sparse.zig").Set(Entity);

pub fn Components(comptime T: type) type {
    return struct {
        values: std.ArrayList(T),
        entities: EntitySet,

        const Self = @This();

        pub fn init(allocator: std.mem.Allocator) Self {
            return Self{
                .values = std.ArrayList(T).init(allocator),
                .entities = EntitySet.init(allocator),
            };
        }

        pub fn deinit(self: *Self) void {
            self.values.deinit();
            self.entities.deinit();
        }

        pub fn getPtr(self: *Self, entity: Entity) ?*T {
            return if (self.entities.indexOf(entity)) |index|
                &self.values.items[index]
            else
                null;
        }

        pub fn put(self: *Self, entity: Entity, component: T) !void {
            if (self.entities.indexOf(entity)) |index| {
                self.values.items[index] = component;
            } else {
                // TODO some errors will leave self in an invalid state

                // TODO Am I assuming the correct index?
                try self.entities.append(entity);
                try self.values.append(component);
            }
        }

        pub fn delete(self: *Self, entity: Entity) void {
            if (self.entities.indexOf(entity)) |index| {
                self.entities.remove(entity);
                _ = self.values.swapRemove(index);
            }
        }
    };
}

test "Components(i32).put, then Components(i32).delete" {
    var components = Components(i32).init(std.testing.allocator);
    defer components.deinit();

    const entity0: Entity = 0;
    const entity1: Entity = 1;

    const expected0: i32 = 42;
    const expected1: i32 = 17;

    try components.put(entity0, expected0);
    try std.testing.expectEqual(@as(usize, 1), components.values.items.len);

    try components.put(entity1, expected1);
    try std.testing.expectEqual(@as(usize, 2), components.values.items.len);

    try std.testing.expectEqual(expected0, components.getPtr(entity0).?.*);
    try std.testing.expectEqual(expected1, components.getPtr(entity1).?.*);

    components.delete(entity0);
    try std.testing.expectEqual(@as(usize, 1), components.values.items.len);

    try std.testing.expectEqual(@as(?*i32, null), components.getPtr(entity0));
    try std.testing.expectEqual(expected1, components.getPtr(entity1).?.*);

    components.delete(entity1);
    try std.testing.expectEqual(@as(usize, 0), components.values.items.len);

    try std.testing.expectEqual(@as(?*i32, null), components.getPtr(entity0));
    try std.testing.expectEqual(@as(?*i32, null), components.getPtr(entity1));
}
