const std = @import("std");

pub fn Slice(comptime T: type) type {
    return struct {
        slice: []const T,

        const Self = @This();

        pub fn init(slice: []const T) Self {
            return Self{ .slice = slice };
        }

        pub fn next(self: *Self) ?T {
            if (self.slice.len == 0) return null;

            defer self.slice = self.slice[1..];
            return self.slice[0];
        }
    };
}

test "Slice(i32).next" {
    var values = &[_]i32{ 42, 99, 0 };

    var iter = Slice(i32).init(values[0..]);

    try std.testing.expectEqual(@as(?i32, 42), iter.next());
    try std.testing.expectEqual(@as(?i32, 99), iter.next());
    try std.testing.expectEqual(@as(?i32, 0), iter.next());
    try std.testing.expectEqual(@as(?i32, null), iter.next());
}

pub fn Yield(comptime I: type) type {
    const msg = "type '" ++ @typeName(I) ++ "' is not an iterator";

    if (!std.meta.trait.hasFn("next")(I)) {
        @compileError(msg ++ ": it needs a function called 'next'");
    }

    const info = @typeInfo(@TypeOf(I.next)).Fn;

    if (info.args.len != 1 or info.args[0].arg_type != *I) {
        @compileError(msg ++ ": 'fn next' needs a parameter of type '" ++ @typeName(*I) ++ "'");
    }

    if (info.return_type) |NextReturnType| {
        return switch (@typeInfo(NextReturnType)) {
            .Optional => |opt| opt.child,
            else => @compileError(msg ++ ": expected 'fn next' to return an optional, found '" ++ @typeName(NextReturnType) ++ "'"),
        };
    } else {
        @compileError(msg ++ ": 'fn next' has no return type");
    }
}

test "Yield" {
    const TestIterator = struct {
        pub fn next(self: *@This()) ?i17 {
            _ = self;
            return null;
        }
    };

    try std.testing.expectEqual(i17, Yield(TestIterator));
}
