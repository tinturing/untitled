const std = @import("std");

const it = @import("iterator.zig");

pub fn Set(comptime T: type) type {
    if (@typeInfo(T) != .Int or @typeInfo(T).Int.signedness != .unsigned) {
        @compileError("'Set' expects an unsigned integer type" ++
            ", found '" ++ @typeName(T) ++ "'");
    }

    const Index = T;

    return struct {
        sparse: std.ArrayList(Index),
        dense: std.ArrayList(T),

        const Self = @This();

        pub fn init(allocator: std.mem.Allocator) Self {
            return Self{
                .sparse = std.ArrayList(Index).init(allocator),
                .dense = std.ArrayList(T).init(allocator),
            };
        }

        pub fn deinit(self: *Self) void {
            self.sparse.deinit();
            self.dense.deinit();
        }

        pub fn indexOf(self: *const Self, value: T) ?Index {
            if (value >= self.sparse.items.len) return null;

            const index = self.sparse.items[value];
            if (index >= self.dense.items.len or self.dense.items[index] != value) return null;

            return index;
        }

        pub fn append(self: *Self, value: T) !void {
            if (self.indexOf(value) != null) return;

            try self.dense.append(value);

            const sparse_index = value;
            const dense_index = @intCast(Index, self.dense.items.len - 1);

            const min_size = @intCast(usize, sparse_index) + 1;
            if (self.sparse.items.len < min_size) {
                try self.sparse.resize(min_size);
            }

            self.sparse.items[sparse_index] = dense_index;
        }

        pub fn remove(self: *Self, value: T) void {
            if (self.indexOf(value)) |index| {
                const last = self.dense.pop();

                if (last != value) {
                    self.dense.items[index] = last;
                    self.sparse.items[last] = index;
                }
            }
        }

        pub const Iterator = it.Slice(T);

        pub fn iter(self: *Self) Iterator {
            return Iterator.init(self.dense.items);
        }
    };
}

pub fn Intersection(comptime I: type) type {
    return struct {
        iter: I,
        set: *const Set(it.Yield(I)),

        const Self = @This();

        pub fn init(iter: I, set: *Set(it.Yield(I))) Self {
            return Self{ .iter = iter, .set = set };
        }

        pub fn next(self: *Self) ?it.Yield(I) {
            return while (self.iter.next()) |current| {
                if (self.set.indexOf(current) != null) {
                    break current;
                }
            } else null;
        }
    };
}

test "Set.append then remove" {
    const Index = u32;

    var set = Set(u32).init(std.testing.allocator);
    defer set.deinit();

    try set.append(17);
    try std.testing.expectEqual(@as(usize, 1), set.dense.items.len);
    try std.testing.expectEqual(@as(usize, 18), set.sparse.items.len);

    try std.testing.expectEqual(@as(?Index, 0), set.indexOf(17));

    try set.append(99);
    try std.testing.expectEqual(@as(usize, 2), set.dense.items.len);
    try std.testing.expectEqual(@as(usize, 100), set.sparse.items.len);

    try std.testing.expectEqual(@as(?Index, 0), set.indexOf(17));
    try std.testing.expectEqual(@as(?Index, 1), set.indexOf(99));

    try set.append(42);
    try std.testing.expectEqual(@as(usize, 3), set.dense.items.len);
    try std.testing.expectEqual(@as(usize, 100), set.sparse.items.len);

    try std.testing.expectEqual(@as(?Index, 0), set.indexOf(17));
    try std.testing.expectEqual(@as(?Index, 1), set.indexOf(99));
    try std.testing.expectEqual(@as(?Index, 2), set.indexOf(42));

    set.remove(99);
    try std.testing.expectEqual(@as(usize, 2), set.dense.items.len);
    try std.testing.expectEqual(@as(usize, 100), set.sparse.items.len);

    try std.testing.expectEqual(@as(?Index, 0), set.indexOf(17));
    try std.testing.expectEqual(@as(?Index, 1), set.indexOf(42));
    try std.testing.expectEqual(@as(?Index, null), set.indexOf(99));

    try set.append(0);
    try std.testing.expectEqual(@as(usize, 3), set.dense.items.len);
    try std.testing.expectEqual(@as(usize, 100), set.sparse.items.len);

    try std.testing.expectEqual(@as(?Index, 0), set.indexOf(17));
    try std.testing.expectEqual(@as(?Index, 1), set.indexOf(42));
    try std.testing.expectEqual(@as(?Index, null), set.indexOf(99));
    try std.testing.expectEqual(@as(?Index, 2), set.indexOf(0));

    set.remove(0);
    try std.testing.expectEqual(@as(usize, 2), set.dense.items.len);
    try std.testing.expectEqual(@as(usize, 100), set.sparse.items.len);

    try std.testing.expectEqual(@as(?Index, 0), set.indexOf(17));
    try std.testing.expectEqual(@as(?Index, 1), set.indexOf(42));
    try std.testing.expectEqual(@as(?Index, null), set.indexOf(99));
    try std.testing.expectEqual(@as(?Index, null), set.indexOf(0));

    set.remove(17);
    try std.testing.expectEqual(@as(usize, 1), set.dense.items.len);
    try std.testing.expectEqual(@as(usize, 100), set.sparse.items.len);

    try std.testing.expectEqual(@as(?Index, 0), set.indexOf(42));
    try std.testing.expectEqual(@as(?Index, null), set.indexOf(17));
    try std.testing.expectEqual(@as(?Index, null), set.indexOf(99));
    try std.testing.expectEqual(@as(?Index, null), set.indexOf(0));

    set.remove(42);
    try std.testing.expectEqual(@as(usize, 0), set.dense.items.len);
    try std.testing.expectEqual(@as(usize, 100), set.sparse.items.len);

    try std.testing.expectEqual(@as(?Index, null), set.indexOf(17));
    try std.testing.expectEqual(@as(?Index, null), set.indexOf(99));
    try std.testing.expectEqual(@as(?Index, null), set.indexOf(42));
    try std.testing.expectEqual(@as(?Index, null), set.indexOf(0));
}

test "Set maximum range" {
    var set = Set(u8).init(std.testing.allocator);
    defer set.deinit();

    var i: u9 = 0;
    while (i <= 255) : (i += 1) {
        try set.append(@intCast(u8, i));
    }

    try std.testing.expectEqual(@as(usize, 256), set.dense.items.len);
    try std.testing.expectEqual(@as(usize, 256), set.sparse.items.len);

    try std.testing.expectEqual(@as(?u8, 0), set.indexOf(0));
    try std.testing.expectEqual(@as(?u8, 255), set.indexOf(255));

    i = 0;
    while (i <= 255) : (i += 1) {
        set.remove(@intCast(u8, i));
    }

    try std.testing.expectEqual(@as(usize, 0), set.dense.items.len);
    try std.testing.expectEqual(@as(usize, 256), set.sparse.items.len);

    try std.testing.expectEqual(@as(?u8, null), set.indexOf(0));
    try std.testing.expectEqual(@as(?u8, null), set.indexOf(255));
}
