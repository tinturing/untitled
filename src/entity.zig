const std = @import("std");

// 4 billion entities ought to be enough for anybody.
pub const Entity = u32;

pub const EntityAllocator = struct {
    count: Entity = 0,
    freed: std.ArrayList(Entity),

    const Error = error{
        NotFound,
        DoubleFree,
        OutOfEntities,
    };

    pub fn init(allocator: std.mem.Allocator) EntityAllocator {
        return EntityAllocator{
            .freed = std.ArrayList(Entity).init(allocator),
        };
    }

    pub fn deinit(self: *EntityAllocator) void {
        self.freed.deinit();
    }

    pub fn alloc(self: *EntityAllocator) !Entity {
        if (self.freed.popOrNull()) |entity| {
            return entity;
        } else if (self.count >= std.math.maxInt(Entity)) {
            return Error.OutOfEntities;
        } else {
            defer self.count += 1;
            return self.count;
        }
    }

    pub fn free(self: *EntityAllocator, entity: Entity) !void {
        if (entity >= self.count) {
            return Error.NotFound;
        }

        if (std.mem.indexOfScalar(Entity, self.freed.items, entity) != null) {
            return Error.DoubleFree;
        }

        try self.freed.append(entity);
    }
};

test "freeing entities allows entities to be reused" {
    var entity_allocator = EntityAllocator.init(std.testing.allocator);
    defer entity_allocator.deinit();

    const e0 = try entity_allocator.alloc();
    const e1 = try entity_allocator.alloc();

    try std.testing.expectEqual(e0, 0);
    try std.testing.expectEqual(e1, 1);

    try entity_allocator.free(e0);
    const e2 = try entity_allocator.alloc();
    const e3 = try entity_allocator.alloc();

    try std.testing.expectEqual(e2, 0);
    try std.testing.expectEqual(e3, 2);
}

test "EntityAllocator.free, prevents double free" {
    var entity_allocator = EntityAllocator.init(std.testing.allocator);
    defer entity_allocator.deinit();

    const e0 = try entity_allocator.alloc();

    try entity_allocator.free(e0);
    const err = entity_allocator.free(e0);

    try std.testing.expectError(EntityAllocator.Error.DoubleFree, err);
}
