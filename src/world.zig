const std = @import("std");

const Entity = @import("entity.zig").Entity;
const EntityAllocator = @import("entity.zig").EntityAllocator;
const Components = @import("component.zig").Components;
const sparse = @import("sparse.zig");
const Intersection = sparse.Intersection;
const EntitySet = sparse.Set(Entity);

/// The `World` containing entities and components.
pub fn World(comptime types: []const type) type {
    // TODO prevent duplicate component types
    const Storage = TupleMap(types, Components);

    return struct {
        storage: Storage,
        entity_allocator: EntityAllocator,

        const Self = @This();

        /// Initializes a new `World`.
        pub fn init(allocator: std.mem.Allocator) Self {
            var storage: Storage = undefined;

            inline for (types) |T, i| {
                storage[i] = Components(T).init(allocator);
            }

            return Self{
                .storage = storage,
                .entity_allocator = EntityAllocator.init(allocator),
            };
        }

        /// Deinitializes a `World`.
        pub fn deinit(self: *Self) void {
            inline for (types) |_, i| {
                self.storage[i].deinit();
            }

            self.entity_allocator.deinit();
        }

        /// Spawns a new `Entity` and `put`s the components in `args` into the world.
        pub fn spawn(self: *Self, args: anytype) !Entity {
            const entity = try self.entity_allocator.alloc();

            inline for (@typeInfo(@TypeOf(args)).Struct.fields) |_, i| {
                try self.put(entity, args[i]);
            }

            return entity;
        }

        /// Releases `entity` from the world.
        pub fn release(self: *Self, entity: Entity) !void {
            inline for (types) |_, i| {
                self.storage[i].delete(entity);
            }

            try self.entity_allocator.free(entity);
        }

        /// This function can be used to bulk-remove entities:
        /// Mark entities with a 'Remove' component, then call this function.
        /// ```
        /// world.releaseAll(Remove)
        /// ```
        pub fn releaseAll(self: *Self, comptime T: type) !void {
            const ti = comptime typeIndex(T);

            const ents = self.storage[ti].entities.dense.items;

            var i: usize = 0;
            while (i < ents.len) : (i += 1) {
                const entity = ents[ents.len - 1 - i];
                try self.release(entity);
            }
        }

        pub fn getPtr(self: *Self, comptime T: type, entity: Entity) ?*T {
            const i = comptime typeIndex(T);

            return self.storage[i].getPtr(entity);
        }

        pub fn put(self: *Self, entity: Entity, value: anytype) !void {
            const i = comptime typeIndex(@TypeOf(value));

            try self.storage[i].put(entity, value);
        }

        pub fn delete(self: *Self, comptime T: type, entity: Entity) void {
            const i = comptime typeIndex(T);

            self.storage[i].delete(entity);
        }

        /// Creates an iterator that visits all entities that have the component
        /// types specified by `query_types`, as well as these components.
        ///
        /// ATTENTION You may not `World.release` entities while you are iterating.
        /// You may, however, `World.delete` components of entities.
        pub fn queryPtr(self: *Self, comptime query_types: []const type) Query(map(query_types, Ptr)) {
            return Query(map(query_types, Ptr)).init(self);
        }

        pub fn query(self: *Self, comptime query_types: []const type) Query(query_types) {
            return Query(query_types).init(self);
        }

        pub fn Query(comptime query_types: []const type) type {
            return CreateUniqueQuery(query_types.len, query_types[0..query_types.len].*);
        }

        /// NOTE `CreateUniqueQuery` is a trick to ensure that, two `Query`s,
        /// with the same types, are treated as equal types by the compiler.
        ///
        /// ```
        /// var query1 = queryPtr(&.{i32, bool};
        /// var query2 = queryPtr(&.{i32, bool};
        /// try std.testing.expectEqual(@TypeOf(query1), @TypeOf(query2));
        /// ```
        ///
        /// The same trick seems to be used in `std.meta.Tuple` with
        /// `std.meta.CreateUniqueTuple`, as mentioned here:
        /// <https://github.com/ziglang/zig/issues/9260>
        fn CreateUniqueQuery(comptime N: comptime_int, comptime query_types: [N]type) type {
            const Result = struct {
                entity: Entity,
                components: TupleMap(query_types[0..], Id),
            };

            // NOTE `unptred` contains the actual types of the components, not
            // pointers or const pointers. This is necessary to access the
            // component array's in `World(...).storage`.
            const unptred = map(query_types[0..], UnPtr);

            // When querying for exactly one component type it is not necessary
            // to use the `EntityIterator` and the `Components(T).get` method.
            // Instead the components and entities can be directly accessed via
            // the `std.ArrayList`s in the `Components(T)` type.
            if (query_types.len == 1) {
                const T = unptred[0];

                return struct {
                    entities: []const Entity,
                    values: []T,

                    const SelfQuery = @This();

                    pub fn init(world: *World(types)) SelfQuery {
                        const index = comptime typeIndex(T);

                        const components = world.storage[index];

                        const vs = components.values.items;
                        const es = components.entities.dense.items;

                        std.debug.assert(vs.len == es.len);

                        return SelfQuery{
                            .values = vs,
                            .entities = es,
                        };
                    }

                    pub fn next(self: *SelfQuery) ?Result {
                        if (self.values.len == 0 or self.entities.len == 0) {
                            return null;
                        }

                        defer self.values = self.values[1..];
                        defer self.entities = self.entities[1..];

                        return Result{
                            .entity = self.entities[0],
                            .components = .{&self.values[0]},
                        };
                    }
                };
            }

            return struct {
                world: *World(types),
                iter: EntityIterator(unptred),

                const SelfQuery = @This();

                pub fn init(world: *World(types)) SelfQuery {
                    return SelfQuery{
                        .world = world,
                        .iter = world.entities(unptred),
                    };
                }

                pub fn next(self: *SelfQuery) ?Result {
                    if (self.iter.next()) |entity| {
                        var result = Result{
                            .entity = entity,
                            .components = undefined,
                        };

                        inline for (unptred) |T, i| {
                            const index = comptime typeIndex(T);
                            // TODO Add an assertion for the nullability of the component
                            result.components[i] = self.world.storage[index].getPtr(entity).?;
                        }

                        return result;
                    } else {
                        return null;
                    }
                }
            };
        }

        /// Creates an iterator that visits all entities that have the component
        /// types specified by `query_types`.
        ///
        /// ATTENTION You may not `World.release` entities while you are iterating.
        /// You may, however, `World.delete` components of entities.
        pub fn entities(self: *Self, comptime query_types: []const type) EntityIterator(query_types) {
            var sets: [query_types.len]*EntitySet = undefined;

            inline for (query_types) |T, i| {
                const index = comptime typeIndex(T);

                sets[i] = &self.storage[index].entities;
            }

            // Moves the shortest set in the first position.
            // Hint: The intersection is at most as big as the shortest set.
            // This is an important optimization to avoid needlessly iterating
            // over long sets.
            if (std.sort.argMin(*EntitySet, sets[0..], {}, lessThan)) |smallest_index| {
                std.mem.swap(*EntitySet, &sets[0], &sets[smallest_index]);
            }

            return intersect(sets.len, sets[0..]);
        }

        fn lessThan(_: void, lhs: *EntitySet, rhs: *EntitySet) bool {
            return lhs.dense.items.len < rhs.dense.items.len;
        }

        fn typeIndex(comptime T: type) usize {
            return std.mem.indexOfScalar(type, types, T) orelse
                @compileError("the type '" ++ @typeName(T) ++ "' is not a component type");
        }
    };
}

pub fn EntityIterator(comptime types: []const type) type {
    if (types.len == 0) @compileError("a query must include at least one component type");

    return Nested(Intersection, EntitySet.Iterator, types.len - 1);
}

/// `Intersection` is an iterator adapter that takes an `EntitySet` and an
/// iterator. The innermost iterator is an `EntitySet.Iterator` which is
/// created from `set[0]`, and not itself an `Intersection`. That's why
/// `EntitySet.Iterator` is wrapped in `Intersection` only `len - 1` times.
fn intersect(
    comptime len: usize,
    sets: *const [len]*EntitySet,
) Nested(Intersection, EntitySet.Iterator, len - 1) {
    return intersectHelper(EntitySet.Iterator, sets[0].iter(), sets.len - 1, sets[1..]);
}

fn intersectHelper(
    comptime Acc: type,
    acc: Acc,
    comptime len: usize,
    sets: *const [len]*EntitySet,
) Nested(Intersection, Acc, len) {
    return switch (len) {
        0 => acc,
        else => intersectHelper(
            Intersection(Acc),
            Intersection(Acc).init(acc, sets[0]),
            len - 1,
            sets[1..],
        ),
    };
}

/// Wraps `T` in `F` a total of `depth` times.
///
/// `Nested(F, T, 0) -> T`
/// `Nested(F, T, 1) -> F(T)`
/// `Nested(F, T, 2) -> F(F(T))`
/// `Nested(F, T, 5) -> F(F(F(F(F(T)))))`
fn Nested(comptime F: fn (type) type, comptime T: type, comptime depth: usize) type {
    return switch (depth) {
        0 => T,
        else => Nested(F, F(T), depth - 1),
    };
}

test "Nested" {
    try std.testing.expectEqual(i32, Nested(Ptr, i32, 0));
    try std.testing.expectEqual(*i32, Nested(Ptr, i32, 1));
    try std.testing.expectEqual(**i32, Nested(Ptr, i32, 2));
    try std.testing.expectEqual(*****i32, Nested(Ptr, i32, 5));
}

test "World.get" {
    var world = World(&.{i32}).init(std.testing.allocator);
    defer world.deinit();

    const comp: i32 = 42;
    const entity = try world.spawn(.{comp});

    const value = world.getPtr(i32, entity);

    try std.testing.expectEqual(comp, value.?.*);
}

test "World.put" {
    var world = World(&.{i32}).init(std.testing.allocator);
    defer world.deinit();

    const comp: i32 = 42;
    const entity = try world.spawn(.{});
    try world.put(entity, comp);
    const value = world.getPtr(i32, entity);

    try std.testing.expectEqual(comp, value.?.*);
}

test "World.delete" {
    var world = World(&.{i32}).init(std.testing.allocator);
    defer world.deinit();

    const comp: i32 = 42;
    const entity = try world.spawn(.{comp});

    try std.testing.expectEqual(comp, world.getPtr(i32, entity).?.*);

    world.delete(i32, entity);

    try std.testing.expectEqual(@as(?*i32, null), world.getPtr(i32, entity));
}

test "World.spawn" {
    var world = World(&.{i32}).init(std.testing.allocator);
    defer world.deinit();

    const e0 = try world.spawn(.{@as(i32, 0)});
    const e1 = try world.spawn(.{@as(i32, 1)});

    var iter = world.entities(&.{i32});

    try std.testing.expectEqual(e0, iter.next().?);
    try std.testing.expectEqual(e1, iter.next().?);
    try std.testing.expectEqual(@as(?Entity, null), iter.next());
}

test "World.release" {
    var world = World(&.{i32}).init(std.testing.allocator);
    defer world.deinit();

    const e0 = try world.spawn(.{@as(i32, 0)});
    const e1 = try world.spawn(.{@as(i32, 1)});

    {
        var iter = world.entities(&.{i32});
        try std.testing.expectEqual(e0, iter.next().?);
        try std.testing.expectEqual(e1, iter.next().?);
        try std.testing.expectEqual(@as(?Entity, null), iter.next());
    }

    try world.release(e0);

    {
        var iter = world.entities(&.{i32});
        try std.testing.expectEqual(e1, iter.next().?);
        try std.testing.expectEqual(@as(?Entity, null), iter.next());
    }

    try world.release(e1);

    {
        var iter = world.entities(&.{i32});
        try std.testing.expectEqual(@as(?Entity, null), iter.next());
    }
}

test "World.releaseAll" {
    const Remove = struct { dummy: u8 = 0 };

    var world = World(&.{ i32, Remove }).init(std.testing.allocator);
    defer world.deinit();

    _ = try world.spawn(.{ @as(i32, 0), Remove{} });
    const e1 = try world.spawn(.{@as(i32, 1)});
    _ = try world.spawn(.{ @as(i32, 2), Remove{} });

    try world.releaseAll(Remove);

    var iter = world.entities(&.{i32});
    try std.testing.expectEqual(@as(?Entity, e1), iter.next());
    try std.testing.expectEqual(@as(?Entity, null), iter.next());
}

test "World.release, then World.get" {
    var world = World(&.{i32}).init(std.testing.allocator);
    defer world.deinit();

    const comp0: i32 = 42;
    const comp1: i32 = 42;
    const entity0 = try world.spawn(.{comp0});
    const entity1 = try world.spawn(.{comp1});

    try std.testing.expectEqual(comp0, world.getPtr(i32, entity0).?.*);
    try std.testing.expectEqual(comp1, world.getPtr(i32, entity1).?.*);

    try world.release(entity0);

    try std.testing.expectEqual(@as(?*i32, null), world.getPtr(i32, entity0));
    try std.testing.expectEqual(comp1, world.getPtr(i32, entity1).?.*);

    try world.release(entity1);

    try std.testing.expectEqual(@as(?*i32, null), world.getPtr(i32, entity0));
    try std.testing.expectEqual(@as(?*i32, null), world.getPtr(i32, entity1));
}

test "World.entities" {
    var world = World(&.{ f32, bool }).init(std.testing.allocator);
    defer world.deinit();

    const e0 = try world.spawn(.{ @as(f32, 3.1415), false });
    const e1 = try world.spawn(.{true});
    const e2 = try world.spawn(.{@as(f32, 2.71828)});

    {
        var iter = world.entities(&.{ bool, f32 });
        try std.testing.expectEqual(e0, iter.next().?);
        try std.testing.expectEqual(@as(?Entity, null), iter.next());
    }

    {
        var iter = world.entities(&.{f32});
        try std.testing.expectEqual(e0, iter.next().?);
        try std.testing.expectEqual(e2, iter.next().?);
        try std.testing.expectEqual(@as(?Entity, null), iter.next());
    }

    {
        var iter = world.entities(&.{bool});
        try std.testing.expectEqual(e0, iter.next().?);
        try std.testing.expectEqual(e1, iter.next().?);
        try std.testing.expectEqual(@as(?Entity, null), iter.next());
    }
}

test "World.Query equality" {
    var world = World(&.{ f32, bool, i32 }).init(std.testing.allocator);
    defer world.deinit();

    var query1 = world.queryPtr(&.{ i32, bool });
    var query2 = world.queryPtr(&.{ i32, bool });
    var query3 = world.query(&.{ *i32, *bool });

    try std.testing.expectEqual(@TypeOf(query1), @TypeOf(query2));
    try std.testing.expectEqual(@TypeOf(query1), @TypeOf(query3));
}

test "World.queryPtr" {
    var world = World(&.{ f32, bool }).init(std.testing.allocator);
    defer world.deinit();

    const e0 = try world.spawn(.{ @as(f32, 3.1415), false });
    const e1 = try world.spawn(.{true});
    const e2 = try world.spawn(.{@as(f32, 2.71828)});

    {
        var iter = world.queryPtr(&.{ bool, f32 });
        const result = iter.next().?;
        try std.testing.expectEqual(e0, result.entity);
        try std.testing.expectEqual(false, result.components[0].*);
        try std.testing.expectEqual(@as(f32, 3.1415), result.components[1].*);
        const result2 = iter.next();
        try std.testing.expectEqual(@as(@TypeOf(result2), null), result2);
    }

    {
        var iter = world.queryPtr(&.{f32});
        const result1 = iter.next().?;
        try std.testing.expectEqual(e0, result1.entity);
        try std.testing.expectEqual(@as(f32, 3.1415), result1.components[0].*);
        const result2 = iter.next().?;
        try std.testing.expectEqual(e2, result2.entity);
        try std.testing.expectEqual(@as(f32, 2.71828), result2.components[0].*);
        const result3 = iter.next();
        try std.testing.expectEqual(@as(@TypeOf(result3), null), result3);
    }

    {
        var iter = world.queryPtr(&.{bool});
        const result1 = iter.next().?;
        try std.testing.expectEqual(e0, result1.entity);
        try std.testing.expectEqual(false, result1.components[0].*);
        const result2 = iter.next().?;
        try std.testing.expectEqual(e1, result2.entity);
        try std.testing.expectEqual(true, result2.components[0].*);
        const result3 = iter.next();
        try std.testing.expectEqual(@as(@TypeOf(result3), null), result3);
    }
}

test "World.query" {
    var world = World(&.{ i32, bool }).init(std.testing.allocator);
    defer world.deinit();

    const e0 = try world.spawn(.{ @as(i32, 0), false });
    const e1 = try world.spawn(.{@as(i32, 1)});
    const e2 = try world.spawn(.{ @as(i32, 2), true });

    {
        var iter = world.query(&.{ *const bool, *i32 });

        while (iter.next()) |result| {
            if (result.components[0].*) {
                result.components[1].* += 40;
            }
        }
    }

    {
        var iter = world.query(&.{*i32});

        const result1 = iter.next().?;
        try std.testing.expectEqual(e0, result1.entity);
        try std.testing.expectEqual(@as(i32, 0), result1.components[0].*);

        const result2 = iter.next().?;
        try std.testing.expectEqual(e1, result2.entity);
        try std.testing.expectEqual(@as(i32, 1), result2.components[0].*);

        const result3 = iter.next().?;
        try std.testing.expectEqual(e2, result3.entity);
        try std.testing.expectEqual(@as(i32, 42), result3.components[0].*);

        const result4 = iter.next();
        try std.testing.expectEqual(@as(@TypeOf(result4), null), result4);
    }
}

/// Applies the `Map` function to every element in `types`, then passes the
/// result to `std.meta.Tuple`.
fn TupleMap(comptime types: []const type, comptime Map: fn (type) type) type {
    return std.meta.Tuple(map(types, Map));
}

test "TupleMap" {
    const types: []const type = &.{ *const f64, bool, *i32 };

    const expected = std.meta.Tuple(&.{ **const f64, *bool, **i32 });
    const actual = TupleMap(types, Ptr);

    try std.testing.expectEqual(expected, actual);
}

fn map(comptime types: []const type, comptime F: fn (type) type) []const type {
    comptime var mapped: [types.len]type = undefined;

    inline for (types) |T, i| {
        mapped[i] = F(T);
    }

    return mapped[0..];
}

test "map" {
    const types: []const type = &.{ f32, *const u8, *bool };

    const expected: []const type = &.{ *f32, **const u8, **bool };
    const actual = map(types, Ptr);

    try comptime std.testing.expectEqualSlices(type, expected, actual);
}

/// Returns its argument `T`.
fn Id(comptime T: type) type {
    return T;
}

test "Id" {
    try std.testing.expectEqual(bool, Id(bool));
    try std.testing.expectEqual(*i32, Id(*i32));
    try std.testing.expectEqual(*const f64, Id(*const f64));
}

/// Returns a type that is a pointer to `T`.
fn Ptr(comptime T: type) type {
    return *T;
}

test "Ptr" {
    try std.testing.expectEqual(*i32, Ptr(i32));
    try std.testing.expectEqual(**const f64, Ptr(*const f64));
}

/// Returns the base type of the pointer type `T`.
///
/// Raises a `@compileError` if `T` is not a pointer type.
fn UnPtr(comptime T: type) type {
    return switch (@typeInfo(T)) {
        .Pointer => |ptr| ptr.child,
        else => @compileError("type '" ++ @typeName(T) ++ "' is not a pointer"),
    };
}

test "UnPtr" {
    try std.testing.expectEqual(i32, UnPtr(*i32));
    try std.testing.expectEqual(f64, UnPtr(*const f64));
}
