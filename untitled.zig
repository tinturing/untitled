pub const Entity = entity.Entity;
pub const Components = component.Components;
pub const World = world.World;

const entity = @import("src/entity.zig");
const component = @import("src/component.zig");
const world = @import("src/world.zig");
